import 'package:flutter/material.dart';
import 'package:flutter_music_project/models/music.api.dart';
import 'package:flutter_music_project/models/music.dart';
import 'package:flutter_music_project/views/widgets/music_card.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late List<Music> _musics;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
  }

  Future<void> getMusics(String artist) async {
    _musics = await MusicApi.getMusics(artist);
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final myController = TextEditingController();
    return Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.music_note),
              SizedBox(width: 10),
              Text('Musique')
            ],
          ),
        ),
        body: _isLoading
            ? Center(
                child: Column(
                children: [
                  TextField(
                    controller: myController,
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(Icons.send),
                        onPressed: (() => myController.text.length > 0
                            ? getMusics(myController.text)
                            : null),
                      ),
                      border: OutlineInputBorder(),
                      hintText: 'Entrer un artiste',
                    ),
                  )
                ],
              ))
            : Column(
                children: [
                  TextField(
                    controller: myController,
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(Icons.send),
                        onPressed: (() => myController.text.length > 0
                            ? getMusics(myController.text)
                            : null),
                      ),
                      border: OutlineInputBorder(),
                      hintText: 'Entrer un artiste',
                    ),
                  ),
                  Expanded(
                      child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: _musics.length,
                    itemBuilder: (context, index) {
                      return MusicCard(
                          title: _musics[index].title,
                          image: _musics[index].image,
                          totalInterest: _musics[index].totalInterest as int);
                    },
                  ))
                ],
              ));
  }
}
