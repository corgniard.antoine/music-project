import 'package:flutter/material.dart';
import 'package:flutter_music_project/views/home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Banque musique',
      theme: ThemeData(
        primaryColor: Colors.white,
        textTheme: TextTheme(
            bodyText2: TextStyle(color: Colors.white)
        ),
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}


