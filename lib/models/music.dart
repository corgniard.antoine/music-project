class Music {
  final String title;
  final String image;
  final int totalInterest;

  Music({required this.title, required this.image, required this.totalInterest});

  factory Music.fromJson(dynamic json) {
    var title = json['title'] as String;
    var urlImg = json['header_image_url'] as String;
    var totalInterest;
    if( json['pyongs_count'] == null){
      totalInterest = 0;
    }
    else{
      totalInterest = json['pyongs_count'];
    }
    return Music(
        title: title,
        image: urlImg,
        totalInterest: totalInterest
    );
  }

  static List<Music> musicsFromSnapshot(List snapshot) {
    return snapshot.map((data) {
      return Music.fromJson(data);
    }).toList();
  }

  @override
  String toString(){
    return 'Music {title: $title, image: $image, totalInterest: $totalInterest}';
  }
}