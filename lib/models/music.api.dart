import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter_music_project/models/music.dart';
import 'dart:convert' as convert;


class MusicApi {
  static Future<List<Music>> getMusics(String artist) async {
    var params = <String, String>{
      "Content-Type": "application/x-www-form-urlencoded",
      "access_token":"mpxb1ZBS7PJ-oN64ZDLP06KreS9zGZNzYgmyRFP7rrtB5hidHdSKUSjP4Rw-g_Uh",
      "q": artist
    };
    var uri = Uri.https('api.genius.com', 'search', params);

    final response = await http.get(uri);

    Map data = jsonDecode(response.body);
    var status = data['meta']['status'];
    List _temp = [];
    for (var i in data['response']['hits']) {
      var result = i['result'];
      _temp.add(result);
    }
    return Music.musicsFromSnapshot(_temp);
  }
}